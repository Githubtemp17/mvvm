package com.example.mvvmfirebase.ui.home

import android.view.View
import androidx.lifecycle.ViewModel
import com.example.mvvmfirebase.Data.repo.UserRepository
import com.example.mvvmfirebase.util.startLoginActivity

class HomeViewModel (
    private val repository: UserRepository

    ) : ViewModel() {

        val user by lazy {
            repository.currentUser()
        }

        fun logout(view: View){
            repository.logout()
            view.context.startLoginActivity()
        }
    }
